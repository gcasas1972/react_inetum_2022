import React from 'react'
import { Text, View } from 'react-native';
class MyComponent extends React.Component {
	//Listing 2.6 Static props
	render() {
		return (
		<BookDisplay book="React Native in Action" />
		)
	}
}
class BookDisplay extends React.Component {
	render() {
		return (
		<View>
			<Text>{ this.props.book }</Text>
		</View>
		)
	}
}
export default BookDisplay;