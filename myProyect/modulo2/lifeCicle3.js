import React from 'react';
import { Text, View } from 'react-native';

class MainComponent extends Component {
	//Listing 2.17 shouldComponentUpdate
	constructor() {
		super()
		this.state = { loading: true, data: {} }
	}
	componentDidMount() {
		setTimeout(() => {
			this.setState({
				loading: false,
				data: {name: 'Nader Dabit', age: 35}
			})
		}, 2000)
	}
	render() {
		if(this.state.loading) {
			return <Text>Loading</Text>
		}
		const { name, age } = this.state.data
		return (
		<View>
			<Text>Name: {name}</Text>
			<Text>Age: {age}</Text>
		</View>
		)
	}
}

export default MainComponent;